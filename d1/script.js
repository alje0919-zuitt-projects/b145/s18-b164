console.log('Hello World');

// Objects

// creating objects using object initializers/literal notation
// {} => Object Literals

let cellphone = {
	name: 'Nokia 3310',
	manufactureDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);

// access using dot notation
console.log(cellphone.name);




// creating objects using a contructor function
// created a reusable function to create several objects that have the same data structure
/*
syntax:
	function objectName(keyA, keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}
*/

function Laptop(name, os, price) {
	this.name = name;
	this.os = os;
	this.price = price;
}

// this is a uniques instance of the Laptop object
// the 'new' operator creates an instance of an object
// object literal
	/*
	let object = {
		key: value
	}
	*/
// instances
	/*
	let object = new object

	*/

let laptop1 = new Laptop('Lenovo', 'Windows 10', 30000);
console.log(laptop1);

// this is another unique instance of the Laptop object
let myLaptop = new Laptop('Macbook Pro', 'Catalina', 50000);


// creating empty objects
let computer = {};
let myComputer = new Object();

// accessing object properties
// using the dot notation
console.log(laptop1.name);

// using the square bracket notation
console.log(laptop1['name']);

// Array of Objects
let array = [laptop1, myLaptop];
console.log(array);

console.log(array[0].name); // Lenovo

// Initializing/Adding/Deleting/Reassigning Object Properties

let car = {};

console.log(car);
// Initializing/adding object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties');
console.log(car);

// Adding object properties using bracket notation
car['manufacture date'] = 2019;
console.log(car);
console.log(car['manufacture date']); // not really in practice

delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);

// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('reassigning properties')
console.log(car);


// Object Methods
// A method is a function which is a property of an object

let person = {
	name: 'John',
	talk: function() {
		console.log(`Hello my name is ${this.name}`)
	}
}

person.talk();
console.log(person);

person.walk = function() {
	console.log(this.name + ' walked 25 steps forward.')
}

person.walk();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function() {
		console.log(`Hello my name is ${this.firstName} ${this.lastName}.
			I live in ${this.address.city}, ${this.address.country}`)
	}
}

friend.introduce();

// scenario
// 1. we would like to create a game that would have seceral pokemon interact with each other
// 2. every pokemon would have the same set of stats, properties and functions


// using object literals (long method)
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduce to _targetPokemonHealth_")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}

// creating an object constructor instead of object literals

function Pokemon(name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	// methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log("targetPokemon's health is now reduced.")
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

// create new instances
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);
rattata.faint();




























