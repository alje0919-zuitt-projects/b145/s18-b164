console.log('Hello World');

// 4.
let trainer = {
	name: 'Chris Adler',
	age: 13,
	pokemon: ['Pikachu', 'Charizard', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	// 5.
	talk: function() {
		console.log(`${this.pokemon[0]}! I choose you!`)
	}
}
console.log(trainer);

// 6.
console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

// 7.
console.log('Result of talk method:');
trainer.talk();

// 8.
function Pokemon(name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 1 * level;


	// methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log
		console.log(target.name + "'s health is now reduced to " + (this.health - this.attack))
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

// create new instances
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);


pikachu.tackle(geodude);
geodude.tackle(pikachu);
geodude.faint();